## This page hosts basic principles for maintaining project based GIT Repositories.

### 1. Creation of repositories

Each project along with its distinct functional parts should have their own repositories. For example we can maintain our code in the following manner -

> CPC-Data-Integration-Mock-UI
    
Will only hosts project files related to the mock UI/UX. Approved code base on this repository will be the starting point of the next phase of the project 

Each collaborator will have access to this repo.

> CPC-Data-Integration-Ang-Dev

This repository will hold FE Angular code integrated with already developed UI. 

Each Angular developer will have access to this repo as collaborator.

> CPC-Data-Integration-Heroku-Dep
 
Only the maintainer of the Heroku will have access to this repo as collaborator. Viewers can have access to see but not to collaborate.


### 2. Cloning

As soon as the repositories are created at GIT - the respective collaborators will be invited to join the repo.

After joining, each collaborator should immediately clone the repo at their local end.

Cloning must be done either through `command-line interface` or using `Desktop Client` software.

Mannual cloning through `.zip` download is strictly prohibited.


### 3. Difference between Cloning & ZIP download

The main idea of using GIT as a version control tool is to allow the collaborators to have access to all the commit histories. This way developers can easily check
the changes they or other developers have made. It is also helpful to pinpoint & debug a specific section of commited code.

Cloning allows to download a `.git` folder in the synced folder at the local end. This folder holds the all the versions and commits for reviewing. Also, new commits 
are stored into this `.git` folder as well.

On the contrary, the `.zip` download is just a copy of the all source codes without any versioning history. Downloading a `.zip` is normally followed by developers 
outside of any team and who does not need to collaborate on the project.


### 4. Commit

Do commit early and often. The "commit" command is used to save your changes to the LOCAL repository. A commit is not automatically transferred to the remote server. 
Using the `git commit` command only saves a new commit object in the local Git repository. Exchanging commits has to be performed manually and explicitly 
(with the `git fetch`, `git pull`, and `git push` commands).

Git only takes full responsibility for your data / code when you commit. If you fail to commit and then do something poorly thought out, you can run into trouble. 
Additionally, having periodic checkpoints means that you can understand how you broke something.

### 5. Push 

The `push` command is used to publish new local commits on a remote server.

The source (i.e. which branch the data should be uploaded from) is always the currently checked out HEAD branch.

The target (i.e. which branch the data should be uploaded to) can be specified in the command's options. 
These options can be omitted, however, if a tracking relationship with a remote branch is set up.

Note that the `push` command can also be used to delete a remote branch.


test commit.


