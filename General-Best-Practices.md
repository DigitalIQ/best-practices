# Coding best practices

### 1. Use consistent indentation
There is no right or wrong indentation that everyone should follow. The best style, is a consistent style. Once you start competing in large projects you’ll immediately understand the importance of consistent code styling.  

For example:
```javascript
function foo() {
    if ($maybe) {
        do_it_now();
        again();
    } else {
        abort_mission();
    }
    finalize();
}
```

### 2. Follow the DRY Principle
DRY stands for “Don’t Repeat Yourself.
The same piece of code should not be repeated over and over again.

### 3. Avoid Deep Nesting
Too many levels of nesting can make code harder to read and follow. 

For example:
```javascript
if (a) {
   …
  if (b) {
      …
    if (c) {
        …
        …
        …
    }
  }
}
```
Can be written as:

```javascript
if (a) {
   return …
}
 
if (b) {
   return …
}  
 
if (c) {
   return …
}  
```
### 4. Limit line length
Long lines are hard to read. It is a good practice to avoid writing horizontally long lines of code.

### 5. File and folder structure
You should avoid writing all of your code in one of 1-2 files. That won’t break your app but it would be a nightmare to read, debug and maintain your application later.

Keeping a clean folder structure will make the code a lot more readable and maintainable.

### 6. Naming conventions
Use of proper naming conventions is a well known best practice. Is a very common issue where developers use variables like X1, Y1 and forget to replace them with meaningful ones, causing confusion and making the code less readable.


### 7. Keep the code simple
The code should always be simple. Complicated logic for achieving simple tasks is something you want to avoid as the logic one programmer implemented a requirement may not make perfect sense to another. So, always keep the code as simple as possible.

### 8. Commenting & Documentation
IDE's (Integrated Development Environment) have come a long way in the past few years. This made commenting your code more useful than ever. Following certain standards in your comments allows IDE's and other tools to utilize them in different ways.

For example:
```javascript
/**
 * Takes two numbers and returns their sum
 * @param a first input to sum
 * @param b second input to sum
 * @returns sum of a and b
 */
function sum(a: number, b: number): number {
    return a + b;
}
```

### 9. Avoid Obvious Comments

Commenting your code is fantastic; however, it can be overdone or just be plain redundant. Take this example:

```php
// get the country code
$country_code = get_country_code($_SERVER['REMOTE_ADDR']);
 
// if country code is US
if ($country_code == 'US') {
 
    // display the form input for state
    echo form_input_state();
}
```
When the text is that obvious, it's really not productive to repeat it within comments.

If you must comment on that code, you can simply combine it to a single line instead:

```php
// display state selection for US users
$country_code = get_country_code($_SERVER['REMOTE_ADDR']);
if ($country_code == 'US') {
    echo form_input_state();
}
```